//
//  MovieGenreListPresenter.swift
//  MovieDBViper
//
//  Created by Arif Rahman Sidik on 14/02/23.
//

import Foundation

protocol MovieGenreListPresenterInput: AnyObject {
    func fetchGenre()
}

protocol MovieGenreListPresenterOutput: AnyObject {
    #warning("Need To Fix the item")
    func presentGenre(item: [String])
}
