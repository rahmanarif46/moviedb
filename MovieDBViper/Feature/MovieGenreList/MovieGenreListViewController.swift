//
//  MovieGenreListViewController.swift
//  MovieDBViper
//
//  Created by Arif Rahman Sidik on 14/02/23.
//

import Foundation
import UIKit

protocol MovieGenreListViewProtocol: AnyObject {
    func showGenre()
}

class MovieGenreListViewController: UIViewController {
    
    var tableView: UITableView!
    
    var movieGenrePresenter: MovieGenreListPresenterInput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func setupTableView(){
        
    }
}

